defmodule Wampex.Router.MixProject do
  use Mix.Project

  def project do
    [
      app: :wampex_router,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      docs: [
        main: "readme",
        extras: ["README.md"],
        assets: "assets"
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      env: [keylen: 32]
    ]
  end

  defp deps do
    [
      {:cluster_kv,
       git: "https://gitlab.com/entropealabs/cluster_kv.git", tag: "75494e6fa3725a1237d423cb3181ceeaf4205bd6"},
      {:cors_plug, "~> 2.0"},
      {:credo, "~> 1.2", only: [:test], runtime: false},
      {:dialyxir, "~> 0.5.1", only: [:test], runtime: false},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:plug_cowboy, "~> 2.1"},
      {:states_language, "~> 0.2"},
      {:wampex, git: "https://gitlab.com/entropealabs/wampex.git", tag: "ae6f2cfcc23dc9af94fa2c375b52bad6c1d0f75d"}
    ]
  end

  defp aliases do
    [
      all_tests: [
        "compile --force --warnings-as-errors",
        "credo --strict",
        "format --check-formatted",
        "dialyzer --halt-exit-status"
      ]
    ]
  end
end

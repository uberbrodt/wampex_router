defmodule Wampex.Router do
  use Supervisor

  alias Wampex.Router.{Realms, REST}
  alias Wampex.Router.Transports.WebSocket

  @spec start_link(
          name: module(),
          port: pos_integer(),
          topologies: [],
          replicas: integer(),
          quorum: integer(),
          authentication_module: module() | map(),
          authorization_module: module() | map()
        ) ::
          {:ok, pid()}
          | {:error, {:already_started, pid()} | {:shutdown, term()} | term()}
  def start_link(
        name: name,
        port: port,
        topologies: topologies,
        replicas: repls,
        quorum: quorum,
        authentication_module: authentication_module,
        authorization_module: authorization_module
      )
      when is_atom(name) do
    Supervisor.start_link(
      __MODULE__,
      {name, port, topologies, repls, quorum, authentication_module, authorization_module},
      name: name
    )
  end

  @spec init({module(), pos_integer(), list(), integer(), integer(), module(), module()}) ::
          {:ok, {:supervisor.sup_flags(), [:supervisor.child_spec()]}} | :ignore
  def init({name, port, topologies, replicas, quorum, authentication_module, authorization_module}) do
    children = [
      {ClusterKV, name: db_name(name), topologies: topologies, replicas: replicas, quorum: quorum},
      {Realms, name: realms_name(name)},
      {Plug.Cowboy,
       scheme: :http,
       plug: REST,
       options: [port: port, dispatch: dispatch(name, authentication_module, authorization_module)]}
    ]

    Supervisor.init(children, strategy: :one_for_one, max_restarts: 10, max_seconds: 10)
  end

  def db_name(name), do: Module.concat([name, KV])
  def realms_name(name), do: Module.concat([name, Realms])

  defp dispatch(name, authentication_module, authorization_module) do
    [
      {:_,
       [
         {"/ws", WebSocket,
          %{
            db: db_name(name),
            name: name,
            authentication_module: authentication_module,
            authorization_module: authorization_module
          }},
         {:_, Plug.Cowboy.Handler, {REST, []}}
       ]}
    ]
  end
end

defmodule Wampex.Router.Authorization do
  @moduledoc false

  @type access_type :: :publish | :subscribe | :register | :call

  @callback authorized?(type :: access_type(), uri :: String.t(), data :: any()) :: true | false
end

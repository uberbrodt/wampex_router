defmodule Wampex.Router.Realms do
  @moduledoc false
  use Supervisor
  alias Wampex.Router.Realms.Proxy
  alias Wampex.Router.Session
  require Logger

  def start_link(name: name) do
    Supervisor.start_link(__MODULE__, name, name: name)
  end

  @impl true
  def init(name) do
    children = [
      {DynamicSupervisor, strategy: :one_for_one, name: realm_supervisor_name(name)},
      {DynamicSupervisor, strategy: :one_for_one, name: session_supervisor_name(name)}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end

  def start_realm(name, realm) do
    r_name = realm_name(realm)
    rs_name = realm_supervisor_name(name)
    DynamicSupervisor.start_child(rs_name, {Proxy, name: r_name})
    r_name
  end

  def start_session(r_name, db, name, transport, socket, atm, azm) do
    s_name = session_supervisor_name(r_name)

    DynamicSupervisor.start_child(
      s_name,
      %{
        id: Session,
        start:
          {Session, :start_link,
           [
             %Session{
               db: db,
               name: name,
               authentication_module: atm,
               authorization_module: azm,
               transport: transport,
               transport_pid: socket
             }
           ]},
        restart: :temporary
      }
    )
  end

  def session_supervisor_name(name), do: Module.concat([name, Sessions])
  def realm_supervisor_name(name), do: Module.concat([name, Supervisor])
  def realm_name(realm), do: Module.concat([__MODULE__, realm])
end

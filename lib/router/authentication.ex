defmodule Wampex.Router.Authentication do
  @moduledoc false

  @type parse_challenge ::
          {authid :: String.t(), authrole :: String.t(), authmethod :: String.t(), authprovider :: String.t()}

  @callback authenticate?(methods :: list(String.t())) :: boolean()
  @callback method() :: String.t()
  @callback challenge(realm :: String.t(), authid :: String.t(), session_id :: integer()) :: map()
  @callback parse_challenge(map()) :: parse_challenge()
  @callback authenticate(
              signature :: String.t(),
              realm :: String.t(),
              authid :: String.t(),
              challenge :: map()
            ) :: {boolean(), any()}
end

defmodule Wampex.Router.REST do
  @moduledoc false
  use Plug.Router
  import Plug.Conn

  plug(CORSPlug, origin: ["*"])
  plug(:match)
  plug(:dispatch)

  get "/favicon.ico" do
    send_resp(conn, 200, "ok")
  end

  get "/alivez" do
    send_resp(conn, 200, "ok")
  end

  get "/readyz" do
    send_resp(conn, 200, "ok")
  end

  match _ do
    send_resp(conn, 404, "oops")
  end
end
